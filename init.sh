#!/bin/bash

I3_CONFIG=${BACKED_UP_HOME}config/i3/

if [ ! -d i3-new-workspace/ ]; then
    echo "i3-new-workspace not found!"
    echo "cloning..."
    git clone https://github.com/kalnar/i3-new-workspace.git  ${I3_CONFIG}i3-new-workspace
fi

mkdir -p ~/.config
rm -rf ~/.config/i3
ln -sf ${I3_CONFIG} ~/.config/i3

